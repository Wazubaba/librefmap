#include <refmap.h>

struct RefMap* rmap_init(struct RefMap* rmap, RefMapCallback func)
{
	unsigned char isAllocated = 0;

	if (rmap == NULL)
	{
		puts("Allocating space for new singleton");
		rmap = malloc(sizeof(struct RefMap));
		if (rmap == NULL)
			return NULL;

		isAllocated = 1;
	}

	rmap->data = NULL;
	rmap->keys = NULL;
	rmap->length = 0;
	rmap->onHeap = isAllocated;
	rmap->cleanupFunction = func;
	return rmap;
}

void* rmap_register(struct RefMap* rmap, const char* key, void* datum)
{
	if (key == NULL || datum == NULL)
		return NULL;

	char** newKeys = realloc(rmap->keys, sizeof(char*) * (rmap->length + 1));
	if (newKeys == NULL)
		return NULL;

	void** newData = realloc(rmap->data, sizeof(void*) * (rmap->length + 1));
	if (newData == NULL)
	{
		free(newKeys);
		return NULL;
	}

	rmap->data = newData;
	rmap->keys = newKeys;
	rmap->data[rmap->length] = datum;
	rmap->keys[rmap->length] = strdup(key);

	rmap->length += 1;

	return rmap->data[rmap->length - 1];
}

void rmap_destroy(struct RefMap* rmap)
{
	if (rmap != NULL)
	{
		unsigned char needsFree = rmap->onHeap;
		
		if (rmap->length > 0)
		{
			size_t itr;
			for (itr = 0; itr < rmap->length; itr++)
			{
				rmap->cleanupFunction(rmap->data[itr]);
				free(rmap->keys[itr]);
			}

			free(rmap->data);
			free(rmap->keys);
		}

		rmap->onHeap = 0;
		rmap->length = 0;

		if (needsFree)
			free(rmap);
	}
}

void* rmap_lookup(struct RefMap* rmap, const char* key)
{
	size_t size = strlen(key);
	size_t itr;
	for (itr = 0; itr < rmap->length; itr++)
	{
		if (strncmp(rmap->keys[itr], key, size) == 0)
			return rmap->data[itr];
	}

	return NULL;
}


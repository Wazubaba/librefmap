#include <stdio.h>
#include <stdlib.h>
#include <refmap.h>

struct TestType
{
	int id;
};

void tt_destroy(void* tt)
{
	struct TestType* ref = (struct TestType*) tt;
	printf("tt_destroy called on testtype with id %d!\n", ref->id);
}

int main(void)
{
	struct RefMap ttman;
	if (rmap_init(&ttman, tt_destroy) == NULL)
	{
		puts("failed to init singleton");
		return EXIT_FAILURE;
	}

	struct TestType tt;
	tt.id = 5;

	rmap_register(&ttman, "test", (void*)&tt);

	void* ttptr;
	
	ttptr = rmap_lookup(&ttman, "test");
	if (ttptr == NULL)
		puts("failed to aquire test item!");
	else
		printf("Got item back with id %d\n", ((struct TestType*) ttptr)->id);


	rmap_destroy(&ttman);

	return 0;
}


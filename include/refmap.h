#ifndef REFMAP_H
#define REFMAP_H

#include <stdio.h>
#include <string.h>
#include <malloc.h>

typedef void (*RefMapCallback)(void*);

struct RefMap
{
	/** Internal array of data */
	void** data;

	/** Internal array of names */
	char** keys;

	/** Whether this container was allocated automatically or not */
	unsigned char onHeap;

	/** Number of elements in the array */
	size_t length;

	/** Callback used on rmap_destroy to free the referenced data itself */
	RefMapCallback cleanupFunction;
};

/**
 * Initialize a RefMap.
 * @param rmap Pointer to a RefMap to initialize
 * @param callback Function to call when rmap_destroy is called. This
 * function will be called once for each datum within the data array.
 * Use this callback to handle your cleanup of the data.
 * @return Pointer to newly-initialized RefMap or NULL on failure
 * @note This function will allocate memory if provided a NULL
 * reference. Regardless of this, you must call rmap_destroy on it.
*/
struct RefMap* rmap_init(struct RefMap* rmap, RefMapCallback);

/**
 * Register a new item with the RefMap.
 * @param rmap Pointer to the RefMap to register with
 * @param key String containing the internal name of the key
 * @param datum Pointer to the data to register with the key
 * @return  Pointer to the newly-registered data or NULL on failure
*/
void* rmap_register(struct RefMap* rmap, const char* key, void* datum);

/**
 * Destroy and release all memory of a RefMap.
 * @param rmap Pointer to the RefMap to destroy
*/
void rmap_destroy(struct RefMap* rmap);

/**
 * Query the RefMap for a reference by name.
 * @param rmap Pointer to the RefMap to query
 * @param key Internal name of the item to look for
 * @return Pointer to the found item or NULL on failure
*/
void* rmap_lookup(struct RefMap* rmap, const char* key);

#endif /* REFMAP_H */


# R E F M A P
I make no claims what-so-ever to this thing's speed, but it basically
implements a [hash table][1] in C.

The search most likely can be improved but at the moment each query
iterates the array to find a match.

I basically wrote this to serve as the internals of singleton resource
managers for my [game engine][2]. I figured implementing the singletons
as wrappers would be a far safer and more preferable system than
trying to rig up some kind of code generation in bash...

[1]:https://en.wikipedia.org/wiki/Hash_table
[2]:https://gitlab.com/Wazubaba/c-engine-prototype


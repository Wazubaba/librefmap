CC= gcc

VERSION_MAJOR= 1
VERSION_MINOR= 0

LIB= librefmap.so
OUTDIR= lib

WARNINGS= -Wcast-align -Wcast-qual -Wmissing-declarations \
					-Wpointer-arith -Wshadow -Wvla -Wwrite-strings -Werror \
					-Wall -Wextra -W

CFLAGS= -Os $(WARNINGS) -fdiagnostics-color=always -Iinclude \
				-Wl,-soname,$(LIB).$(VERSION_MAJOR)

OCFLAGS= -Os $(WARNINGS) -fdiagnostics-color=always -Iinclude

SRC= $(shell find src -type f -name "*.c")
OBJ= $(patsubst %.c, %.o, $(SRC))

.PHONY: all clean test

all: $(LIB)

test:
	$(MAKE) -f test/Makefile

%.o: %.c
	$(CC) -c -fPIC $(OCFLAGS) -o$@ $<

$(LIB): $(OBJ)
	mkdir $(OUTDIR)
	$(CC) -fPIC -shared $(CFLAGS) -o$(OUTDIR)/$(LIB) $^
	cd $(OUTDIR) && \
		ln -s $(LIB) $(LIB).$(VERSION_MAJOR) && \
		ln -s $(LIB) $(LIB).$(VERSION_MAJOR).$(VERSION_MINOR)

clean:
	-$(RM) $(OBJ)
	-$(RM) -r $(OUTDIR)
	$(MAKE) -f test/Makefile clean

